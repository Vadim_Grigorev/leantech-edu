package com.eurekademo.eurekacleint2.storage;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class Request {
    private String name;
    private int task;
}
