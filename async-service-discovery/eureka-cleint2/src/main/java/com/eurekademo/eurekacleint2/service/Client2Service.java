package com.eurekademo.eurekacleint2.service;

import com.eurekademo.eurekacleint2.storage.Request;
import com.eurekademo.eurekacleint2.storage.RequestStorage;
import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class Client2Service {
    private final DiscoveryClient discoveryClient;
    private final RequestStorage storage;

    @Async
    public void doWork(Integer value) throws InterruptedException {
        System.out.println("Doing work");

        UUID id = storage.add(new Request("Some_Request",value));

        value = storage.get(id).getTask() + 42;

        Thread.sleep(20000);

        ServiceInstance instance = discoveryClient.getInstances("spring-cloud-eureka-client").get(0);
        WebClient client = WebClient.create(instance.getUri().toString());

        client.post()
                .uri("/receiveTask")
                .bodyValue(value)
                .retrieve()
                .bodyToMono(Integer.class)
                .block();

        System.out.println("Value sent : " + value);
    }
}

//
//        WebClient webClient = WebClient.create(service.getUri().toString());
//
//        WebClient.UriSpec<WebClient.RequestBodySpec> uriSpec = client.method(HttpMethod.GET);
//
//        WebClient.RequestBodySpec bodySpec = uriSpec.uri("/greeting");
//
//        WebClient.RequestHeadersSpec<?> headersSpec = bodySpec.bodyValue("text");
//
//        Mono<String> response = headersSpec.retrieve().bodyToMono(String.class);

//        Mono<String> response = headersSpec.exchangeToMono(response -> {
//            if (response.statusCode().equals(HttpStatus.OK)) {
//                return response.bodyToMono(String.class);
//            } else if (response.statusCode().is4xxClientError()) {
//                return Mono.just("Error response");
//            } else {
//                return response.createException()
//                        .flatMap(Mono::error);
//            }
//        });

//        final String uri = service.getUri() + "/greeting" ;
//
//        RestTemplate restTemplate = new RestTemplate();
//        String result = restTemplate.getForObject(uri, String.class);
//
//        return result + " from another client";
