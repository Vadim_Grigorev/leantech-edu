package com.eurekademo.eurekacleint2.controller;

import com.eurekademo.eurekacleint2.service.Client2Service;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.net.URISyntaxException;

@RestController
@RequiredArgsConstructor
public class Client2Controller {
    private final Client2Service client2Service;

    @PostMapping("/doWork")
    public void doWork(@RequestBody Integer value) throws InterruptedException {
        System.out.println("work taken " + value);
        client2Service.doWork(value);
    }
}
