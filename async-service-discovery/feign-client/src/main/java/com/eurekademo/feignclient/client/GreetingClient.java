package com.eurekademo.feignclient.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("spring-cloud-eureka-client2")
public interface GreetingClient {
    @RequestMapping("/greeting")
    String greeting();
}
