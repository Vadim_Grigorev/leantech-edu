package com.eurekademo.eurekacleint2.storage;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
@NoArgsConstructor
public class RequestStorage {
    private Map<UUID, Request> storage = new HashMap<>();

    public UUID add(Request request) {
        UUID id = UUID.randomUUID();
        storage.put(id, request);
        return id;
    }

    public Request get(UUID id) {
        return storage.get(id);
    }

    public void update(Request request, UUID id) {
        storage.replace(id, request);
    }
}
