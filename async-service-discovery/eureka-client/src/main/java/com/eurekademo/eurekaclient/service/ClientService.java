package com.eurekademo.eurekaclient.service;

import com.netflix.discovery.EurekaClient;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class ClientService {
    private Map<Integer, UUID> results = new HashMap<>();
    private final DiscoveryClient discoveryClient;

    public void sendTask(Integer value) throws InterruptedException {
        ServiceInstance instance = discoveryClient.getInstances("spring-cloud-eureka-client2").get(0);
        WebClient client = WebClient.create(instance.getUri().toString());

        client.post()
                .uri("/doWork")
                .bodyValue(value)
                .retrieve()
                .bodyToMono(Integer.class)
                .block();
    }

    public void receiveTask(Integer value) {
        results.put(value, UUID.randomUUID());
        System.out.println(value);
    }

    public String showTask(Integer value) {
        System.out.println(value);
        return results.get(value).toString();
    }
}
