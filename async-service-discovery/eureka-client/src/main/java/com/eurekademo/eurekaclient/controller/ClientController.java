package com.eurekademo.eurekaclient.controller;

import com.eurekademo.eurekaclient.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;

    @PostMapping("/sendTask")
    public void sendTask(@RequestBody Integer value) throws InterruptedException {
        clientService.sendTask(value);
    }

    @PostMapping("/receiveTask")
    public void receiveTask(@RequestBody Integer value) {
        clientService.receiveTask(value);
    }

    @GetMapping("/showTask/{value}")
    public String showTask(@PathVariable Integer value) {
        return clientService.showTask(value);
    }
}
